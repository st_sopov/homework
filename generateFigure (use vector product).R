isLeftTripleOfVectors <- function (vector1,vector2)  {
  vector1[1]*vector2[2] <= vector2[1]*vector1[2]
}

isPointInFigure <- function (point,pointsFigure)  {
   vectors <- pointsFigure-point
   n <- ncol(vectors)
   # check all pairs of vectors
   result <- c( sapply (seq(n-1), function (i) isLeftTripleOfVectors( vectors[,i],vectors[,i+1] )),
      isLeftTripleOfVectors(vectors[,n],vectors[,1] ))
   if ( sum(result) < length(result) )
     FALSE
   else  
     TRUE
}

contour <- cbind(c(1,1),c(15,30),c(50,50))
point <- c(5,5)
res <- isPointInFigure(point,contour)
res
 
X <- matrix (1:50,50,50,byrow=TRUE)
Y <- matrix (50:1,50,50)

c(X[1,1],Y[1,1])
Matrix <- sapply (seq(nrow(X)), function(i) sapply(seq(ncol(Y)), function(j) isPointInFigure(c(X[i,j],Y[i,j]),contour)))
image(Matrix)
