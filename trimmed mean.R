getTrim <- function (vector,percent)  {
 v <- sort(vector) 
 borderIndex <- floor(percent * length(v))
 mean(v[seq(borderIndex+1,length(v)-borderIndex)])
}